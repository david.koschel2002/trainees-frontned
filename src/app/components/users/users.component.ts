import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  userTitle = 'Total Users';
  username: string;
  imgUrl = 'https://desarrolloweb.com/archivoimg/general/4365.jpg';

  users: User[] = [{ name: 'Jose', age: 28}, { name: 'Jose2', age: 25}];

  constructor() { 
    this.username = 'Jose';
  }

  ngOnInit() {
  }

  onClick(user: User) {
    console.log(user);
  }

}
